<?php

return [
    'adminEmail'  => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName'  => 'Example.com mailer',
    'apiCred'     => [
        'cred1' => [
            'apiKey' => 'QWE1123',
            'apiId'  => 'first',
        ],
        'cred2' => [
            'apiKey' => 'QeRW23123',
            'apiId'  => 'second',
        ],
    ],
    'redis'       => [
        'host' => '127.0.0.1'
    ]
];
