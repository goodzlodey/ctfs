<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

class ApiModel extends ActiveRecord
{
    public static function tableName(): string
    {
        return 'api';
    }

    public function rules(): array
    {
        return [
            [['key1', 'key2', 'value', 'createdAt'], 'safe']
        ];
    }

    public static function getRecords($select = '*', array $where = [], array $params = [], array $options = []): array
    {
        $query = new Query();
        $query
            ->select($select)
            ->from(self::tableName())
            ->where(implode(' AND ', $where), $params);

        if (isset($options['limit'])) {
            $query->limit($options['limit']);
        }

        if (isset($options['orderBy'])) {
            $query->orderBy($options['orderBy']);
        }

        return $query->all();
    }

    public static function getCountRecords(array $where = [], array $params = []): int
    {
        $query = new Query();
        $query
            ->select('count(id)')
            ->from(self::tableName())
            ->where(implode(' AND ', $where), $params);

        return $query->count();
    }

    public static function deleteRecordsByDate(string $date, int $limit)
    {
        return Yii::$app->db->createCommand("
        DELETE FROM `api` WHERE createdAt<='$date' LIMIT $limit;
        ")->execute();
    }

}
