<?php


namespace app\models;


use Redis;

class RateLimiter extends Redis
{
    //Адекватной аналитики по вопросам параметров ограничения не особо нашел
    // + мне кажется, что тут надо отталкиваться от возможностей сервера, базы и,
    // хотя бы, общей картины кол-ва пользователей, которые, теоретически, могу стучаться

    // Сам лимит проверяется в три этапа по трем показателям:
    //     - общий счетчик запросов
    //     - счетчик запросов по APIKEY+APIID
    //     - счетчик запросов по IP
    // Логика была в том, что бы защититься от попытки уронить завалив реквестами по разным APIKEY и APIID
    // и было бы не важно делают это с одного IP или разных

    private const GLOBAL_KEY_NAME = 'global';
    private const LIMIT_GLOBAL    = 200;
    private const LIMIT_PER_CRED  = 20;
    private const LIMIT_PER_IP    = 10;
    private const TIMEOUT         = 1;

    private string $requestIP;

    public function __construct()
    {
        parent::__construct();
        $this->connect(\Yii::$app->params['redis']['host']);
        $this->requestIP = $_SERVER['REMOTE_ADDR'];
    }

    /**
     * @param string $key
     * @return bool
     */
    public function checkRateLimit(string $key): bool
    {
        $valueGlobal = $this->get(self::GLOBAL_KEY_NAME);
        $valueByCred = $this->get($key);
        $valueByIp   = $this->get($this->requestIP);

        if (
            (false !== $valueGlobal && $valueGlobal >= self::LIMIT_GLOBAL) ||
            (false !== $valueByCred && $valueByCred >= self::LIMIT_PER_CRED) ||
            (false !== $valueByIp   && $valueByIp   >= self::LIMIT_PER_IP)
        ) {
            return false;
        }
        $this->setRateLimit($key);
        return true;
    }

    /**
     * @param string $key
     */
    public function setRateLimit(string $key): void
    {
        $this->set(self::GLOBAL_KEY_NAME, 0, ['NX', 'EX' => self::TIMEOUT]);
        $this->set($key, 0, ['NX', 'EX' => self::TIMEOUT]);
        $this->set($this->requestIP, 0, ['NX', 'EX' => self::TIMEOUT]);
        $this->incr(self::GLOBAL_KEY_NAME);
        $this->incr($key);
        $this->incr($this->requestIP);
    }
}