<?php

namespace app\commands;

use app\models\ApiModel;
use Exception;
use Yii;
use yii\base\ExitException;
use yii\console\Controller;
use yii\db\Expression;
use yii\helpers\BaseConsole;

class JanitorController extends Controller
{
    public bool $fastClean = false;

    /**
     * @param string $actionID
     * @return string[]
     */
    public function options($actionID): array
    {
        return ['fastClean'];
    }

    /**
     * @return array|string[]
     */
    public function optionAliases(): array
    {
        return ['f' => 'fastClean'];
    }

    /**
     * Cleanup old data from api table. To skip re-confirming the deletion use -f key.
     * @throws ExitException
     */
    public function actionCleaning(): void
    {
        $this->stdout("Janitor greets you!\n\n");
        $days = (int)BaseConsole::input("Number of relevance days (records older than this will be deleted): ");
        $date = date('Y-m-d', strtotime("- $days days"));
        $records = $this->getRandRecords($date);
        if (empty($records)) {
            $this->stdout("There are no records to delete... :(\n");
            Yii::$app->end(0);
        }
        $this->printRecords($records);

        if (!$this->fastClean) {
            $delete = $this->confirm("Are you sure you want to delete those records?");
        }
        if ($this->fastClean || $delete) {
            $this->deleteRecords($date);
        } else {
            $this->stdout("As you wish...Bye!\n");
            Yii::$app->end(0);
        }
        $this->stdout("\nCall me, if you'll need to clean some shi...uuum, you know what I mean. \nGood luck, Bye!\n");
        Yii::$app->end(0);
    }

    /**
     * @param string $date
     * @throws Exception
     */
    protected function deleteRecords(string $date): void
    {
        $this->stdout("Processing...\n");
        $recordsCount = $this->getRecordsCount($date);
        $seconds = random_int(5, 10);
        $recordsToDelete = ($recordsCount >= $seconds) ? (int)($recordsCount / $seconds) : $recordsCount;
        for ($i = 0; $i < $seconds; $i++) {
            if (($seconds - 1) === $i) {
                if ($recordsCount !== 0) {
                    ApiModel::deleteRecordsByDate($date, $recordsCount);
                }
            } else {
                if ($recordsCount >= $recordsToDelete) {
                    $recordsCount -= $recordsToDelete;
                }
                ApiModel::deleteRecordsByDate($date, $recordsToDelete);
            }
            $timeleft = $i === $seconds ? 1 : $seconds - $i;
            $this->stdout($this->getProgressBar($i, $seconds, " Time left: " . $timeleft . " sec."));
            sleep(1);
        }
        $this->stdout($this->getProgressBar($seconds, $seconds, " I'm done!"));
    }

    /**
     * @param $done
     * @param $total
     * @param string $info
     * @param int $width
     * @return string
     */
    protected function getProgressBar($done, $total, $info = "", $width = 50): string
    {
        $percent = round(($done * 100) / $total);
        $bar = round(($width * $percent) / 100);
        return sprintf("%s%%[%s>%s]%s\n", $percent, str_repeat("=", $bar), str_repeat(" ", $width - $bar), $info);
    }

    /**
     * @param array $records
     */
    protected function printRecords(array $records): void
    {
        $indent = '    ';
        if (count($records) === 1) {
            $this->stdout("\nThere is only one record to delete: \n");
        } else {
            $this->stdout("\nRandom " . count($records) . " records to delete: \n");
        }
        foreach ($records as $key => $record) {
            $this->stdout($key + 1 . "\n");
            foreach ($record as $name => $value) {
                $this->stdout("$indent $name => $value\n");
            }
        }
        $this->stdout("\n");
    }

    /**
     * @param string $date
     * @return array
     */
    protected function getRandRecords(string $date): array
    {
        return ApiModel::getRecords(
            'key1, key2, value, createdAt',
            [
                'createdAt <= :date'
            ],
            [
                ':date' => $date
            ],
            [
                'limit' => 5,
                'orderBy' => new Expression('RAND()')
            ]
        );
    }

    /**
     * @param $date
     * @return int
     */
    protected function getRecordsCount($date): int
    {
        return ApiModel::getCountRecords(
            [
                'createdAt <= :date'
            ],
            [
                ':date' => $date
            ]
        );
    }
}
