<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%api}}`.
 */
class m200804_105923_create_api_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('api', [
            'id'        => $this->primaryKey(),
            'key1'      => $this->string(20)->notNull(),
            'key2'      => $this->string(32)->notNull(),
            'value'     => $this->json()->notNull(),
            'createdAt' => $this->dateTime()->notNull()
        ]);
        $this->createIndex('keys', 'api', ['key1', 'key2']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%api}}');
    }
}
