<?php

namespace app\forms;

use yii\base\Model;

class ApiForm extends Model
{
    public const KEY1_PHONE = 'phone';
    public const KEY1_EMAIL = 'email';

    public const SCENARIO_ADD = 'add';
    public const SCENARIO_GET = 'get';

    public $key1;
    public $key2;
    public $value;
    public $createdAt;

    public function rules(): array
    {
        return [
            [['key1', 'key2', 'value'], 'required', 'on' => [self::SCENARIO_ADD]],
            [['key1', 'key2'], 'required', 'on' => [self::SCENARIO_GET]],
            [['key1', 'key2'], 'trim', 'on' => [self::SCENARIO_ADD]],
            ['key1', 'in', 'range' => [self::KEY1_PHONE, self::KEY1_EMAIL], 'on' => [self::SCENARIO_ADD, self::SCENARIO_GET]],
            ['key2', 'string', 'max' => 32, 'on' => [self::SCENARIO_ADD, self::SCENARIO_GET]],
            ['key2', 'email', 'when' => function () {
                return $this->key1 === self::KEY1_EMAIL;
            }, 'on' => [self::SCENARIO_ADD, self::SCENARIO_GET], 'message' => 'Invalid email format!'],
            ['key2', 'validatePhoneNumber', 'when' => function () {
                return $this->key1 === self::KEY1_PHONE;
            }, 'on' => [self::SCENARIO_ADD, self::SCENARIO_GET]],
            [['value', 'createdAt'], 'safe', 'on' => [self::SCENARIO_ADD]]
        ];
    }

    public function validatePhoneNumber($attribute): void
    {
        if (!preg_match('~\+\d{12}~', $this->key2)) {
            $this->addError($attribute, 'Phone number should be in format +xxxxxxxxxxxx !');
        }
    }
}
