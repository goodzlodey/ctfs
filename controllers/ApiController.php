<?php

namespace app\controllers;

use app\forms\ApiForm;
use app\models\ApiModel;
use app\models\RateLimiter;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class ApiController extends Controller
{
    protected ?array $data;

    public function beforeAction($action): bool
    {
        if (Yii::$app->request->isPost) {
            $this->data = Yii::$app->request->post();
        } else {
            $this->data = Yii::$app->request->get();
        }

        $this->checkAccess();

        $this->data['createdAt'] = date('Y-m-d H:i:s');
        return parent::beforeAction($action);
    }

    protected function checkAccess(): void
    {
        if (isset($this->data['apiKey'], $this->data['apiId'])) {
            $rateLimiter = new RateLimiter();
            $params = array_column(Yii::$app->params['apiCred'], 'apiId', 'apiKey');

            if ($rateLimiter->checkRateLimit($this->data['apiKey'] . $this->data['apiId'])) {
                if (!isset($params[$this->data['apiKey']]) || $params[$this->data['apiKey']] !== $this->data['apiId']) {
                    echo json_encode(['code' => 403, 'response' => 'Authorization credentials are invalid!']);
                    Yii::$app->end(0);
                }
            } else {
                echo json_encode(['code' => 429, 'response' => 'Too Many Requests!']);
                Yii::$app->end(0);
            }
        } else {
            echo json_encode(['code' => 403, 'response' => 'Authorization credentials are invalid!']);
            Yii::$app->end(0);
        }
    }

    public function actionSetData(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new ApiModel();
        $form  = new ApiForm();
        $form->scenario = ApiForm::SCENARIO_ADD;

        $form->setAttributes($this->data);
        if ($form->validate()) {
            $model->setAttributes($this->data);
            $model->save();
        } else {
            return ['code' => 400, 'response' => [$form->getErrors()]];
        }

        return ['code' => 200, 'response' => 'Data was successfully saved!'];
    }

    public function actionGetData(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $form = new ApiForm();
        $form->scenario = ApiForm::SCENARIO_ADD;

        $form->setAttributes($this->data);
        if ($form->validate()) {
            $response = ApiModel::findAll([
                'key1' => $this->data['key1'],
                'key2' => $this->data['key2'],
            ]);
        } else {
            return ['code' => 400, 'response' => [$form->getErrors()]];
        }

        return ['code' => 200, 'response' => $response];
    }
}
